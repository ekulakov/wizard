import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { Country } from "../models/country";
import { Province } from "../models/province";
import { CreateUserRequest } from "../models/create-user-request";
import { CreateUserResponse } from "../models/create-user-response";
import { ValidateEmailRequest } from "../models/validate-email-request";
import { ValidateEmailResponse } from "../models/validate-email-response";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(
    private _http: HttpClient,
  ) {
  }

  getCountries(): Observable<Country[]> {
    return this._http.get<Country[]>('api/countries');
  }

  getCountryProvinces(countryId: number): Observable<Province[]> {
    return this._http.get<Province[]>(`api/countries/${countryId}/provinces`);
  }

  createUser(user: CreateUserRequest): Observable<CreateUserResponse> {
    return this._http.post<CreateUserResponse>('api/users/create', user);
  }

  validateEmail(request: ValidateEmailRequest): Observable<ValidateEmailResponse> {
    return this._http.post<ValidateEmailResponse>('api/users/validate-email', request);
  }
}
