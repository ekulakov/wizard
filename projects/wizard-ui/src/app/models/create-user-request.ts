export class CreateUserRequest {
  email: string;
  password: string;
  passwordConfirm: string;
  provinceId: number;
}
