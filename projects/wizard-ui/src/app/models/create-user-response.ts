export class CreateUserResponse {
  id: number;
  email: string;
}
