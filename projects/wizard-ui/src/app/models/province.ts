export class Province {
  id: number;
  countryId: number;
  name: string;
}
