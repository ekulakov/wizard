import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable, Subject } from "rxjs";

import { Country } from "./models/country";
import { Province } from "./models/province";
import { DataService } from "./services/data.service";
import { CreateUserRequest } from "./models/create-user-request";
import { emailAsyncValidator } from "./validators/email-async-validator";
import { passwordValidator } from "./validators/password-validator";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  hidePassword = true;
  hidePasswordConfirm = true;

  stepOneFormGroup: FormGroup;
  stepTwoFormGroup: FormGroup;

  emailControl: AbstractControl;
  passwordControl: AbstractControl;
  passwordConfirmControl: AbstractControl;
  agreementControl: AbstractControl;
  countryControl: AbstractControl;
  provinceControl: AbstractControl;

  countries$: Observable<Country[]>;
  provinces$: Observable<Province[]>;

  message: string;
  details: any;

  private _destroyed = new Subject();
  private _selectedCountry: number;

  set selectedCountry(id: number) {
    this._selectedCountry = id;
    this.provinces$ = this._dataService.getCountryProvinces(id);
  }

  get selectedCountry(): number {
    return this._selectedCountry;
  }

  constructor(
    private _formBuilder: FormBuilder,
    private _dataService: DataService,
  ) {
  }

  ngOnInit() {
    this.countries$ = this._dataService.getCountries();

    this.stepOneFormGroup = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email], [emailAsyncValidator(this._dataService)]],
      password: ['', [Validators.required, Validators.pattern(/[0-9]/), Validators.pattern(/[a-zA-z]/)]],
      passwordConfirm: ['', [passwordValidator]],
      agreement: ['', Validators.requiredTrue],
    });

    this.emailControl = this.stepOneFormGroup.get('email');
    this.passwordControl = this.stepOneFormGroup.get('password');
    this.passwordConfirmControl = this.stepOneFormGroup.get('passwordConfirm');
    this.agreementControl = this.stepOneFormGroup.get('agreement');

    this.stepTwoFormGroup = this._formBuilder.group({
      country: ['', Validators.required],
      province: ['', Validators.required],
    });

    this.countryControl = this.stepTwoFormGroup.get('country');
    this.provinceControl = this.stepTwoFormGroup.get('province');
  }

  ngOnDestroy(): void {
    this._destroyed.next();
    this._destroyed.complete();
  }

  submit() {
    if (!this.stepOneFormGroup.valid) {
      return;
    }

    if (!this.stepTwoFormGroup.valid) {
      return;
    }

    const createUserRequest = new CreateUserRequest();
    createUserRequest.email = this.emailControl.value;
    createUserRequest.password = this.passwordControl.value;
    createUserRequest.passwordConfirm = this.passwordConfirmControl.value;
    createUserRequest.provinceId = this.provinceControl.value;

    this._dataService.createUser(createUserRequest)
      .subscribe(user => {
          this.message = "Success";
          this.details = user;
        },
        e => {
          this.message = "Failed";
          this.details = e.error;
        });
  }
}
