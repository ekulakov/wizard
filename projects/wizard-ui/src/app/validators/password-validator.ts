import { AbstractControl, FormGroup, ValidationErrors } from "@angular/forms";

export const passwordValidator = (control: AbstractControl): ValidationErrors | null => {
  const parent = <FormGroup>control.parent;
  if (!parent) {
    return null;
  }

  const condition = control.value !== parent.get('password').value;
  return condition ? { passwordsAreNotEqual: true } : null;
};
