import { DataService } from "../services/data.service";
import { FormControl} from "@angular/forms";
import { debounceTime, map, switchMap, tap } from "rxjs/operators";

export const emailAsyncValidator =
  (dataService: DataService, time: number = 500) => {
    return (input: FormControl) => {
      return input.valueChanges.pipe(
        debounceTime(time),
        switchMap(() => dataService.validateEmail({ email: input.value })),
        map(response => {
          return response.isFree ? null : { emailTaken: true };
        }),
        tap(x => input.setErrors(x)),
      );
    };
  };
