FROM mcr.microsoft.com/dotnet/core/sdk:2.2
WORKDIR /source

COPY ./src/Wizard.Api/Wizard.Api.csproj ./src/Wizard.Api/Wizard.Api.csproj
COPY ./src/Wizard.DataAccess/Wizard.DataAccess.csproj ./src/Wizard.DataAccess/Wizard.DataAccess.csproj
COPY ./src/Wizard.Domain/Wizard.Domain.csproj ./src/Wizard.Domain/Wizard.Domain.csproj
COPY ./src/Wizard.Api.Tests/Wizard.Api.Tests.csproj ./src/Wizard.Api.Tests/Wizard.Api.Tests.csproj

COPY ./Wizard.sln .

RUN dotnet restore Wizard.sln

COPY ./src ./src

ARG CONFIGURATION=Release
RUN dotnet build Wizard.sln -c ${CONFIGURATION} --no-restore