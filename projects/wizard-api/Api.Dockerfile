FROM wizard-build AS builder
WORKDIR /source

ARG CONFIGURATION=Release
RUN dotnet publish --output /app --configuration ${CONFIGURATION} ./src/Wizard.Api --no-restore

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app

ENV DOCKERIZE_VERSION v0.6.1
RUN curl -L https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz | tar -xzC /usr/local/bin

COPY --from=builder /app .
ENTRYPOINT ["/usr/bin/dotnet", "/app/Wizard.Api.dll"]