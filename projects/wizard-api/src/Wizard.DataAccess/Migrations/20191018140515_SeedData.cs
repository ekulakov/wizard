﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wizard.DataAccess.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "countries",
                columns: new[] { "id", "name" },
                values: new object[,]
                {
                    { 1, "Country 1" },
                    { 2, "Country 2" }
                });

            migrationBuilder.InsertData(
                table: "provinces",
                columns: new[] { "id", "country_id", "name" },
                values: new object[,]
                {
                    { 1, 1, "Province 1" },
                    { 2, 1, "Province 2" },
                    { 3, 2, "Province 3" },
                    { 4, 2, "Province 4" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "provinces",
                keyColumn: "id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "provinces",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "provinces",
                keyColumn: "id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "provinces",
                keyColumn: "id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "countries",
                keyColumn: "id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "countries",
                keyColumn: "id",
                keyValue: 2);
        }
    }
}
