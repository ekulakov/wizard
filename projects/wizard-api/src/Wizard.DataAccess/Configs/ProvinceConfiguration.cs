using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Wizard.Domain;

namespace Wizard.DataAccess.Configs
{
    public class ProvinceConfiguration : IEntityTypeConfiguration<Province>
    {
        public void Configure(EntityTypeBuilder<Province> builder)
        {
            builder.Property(x => x.Id).ValueGeneratedNever();
            
            builder.HasData(new Province {Id = 1, CountryId = 1, Name = "Province 1"});
            builder.HasData(new Province {Id = 2, CountryId = 1, Name = "Province 2"});
            builder.HasData(new Province {Id = 3, CountryId = 2, Name = "Province 3"});
            builder.HasData(new Province {Id = 4, CountryId = 2, Name = "Province 4"});
        }
    }
}