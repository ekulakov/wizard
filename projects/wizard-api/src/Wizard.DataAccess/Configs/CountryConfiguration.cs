using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Wizard.Domain;

namespace Wizard.DataAccess.Configs
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.Property(x => x.Id).ValueGeneratedNever();

            builder.HasData(new Country {Id = 1, Name = "Country 1"});
            builder.HasData(new Country {Id = 2, Name = "Country 2"});
        }
    }
}