namespace Wizard.Api.Models
{
    public class ValidateEmailRequest
    {
        public string Email { get; set; }
    }
}