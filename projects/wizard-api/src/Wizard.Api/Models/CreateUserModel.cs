namespace Wizard.Api.Models
{
    public class CreateUserModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string PasswordConfirm { get; set; }
        
        public int ProvinceId { get; set; }
    }
}