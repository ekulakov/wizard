namespace Wizard.Api.Models
{
    public class ProvinceListModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}