namespace Wizard.Api.Models
{
    public class CountryListModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}