namespace Wizard.Api.Models
{
    public class ValidateEmailResponse
    {
        public bool IsFree { get; set; }
    }
}