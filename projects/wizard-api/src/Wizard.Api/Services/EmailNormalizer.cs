using System;

namespace Wizard.Api.Services
{
    internal class EmailNormalizer : IEmailNormalizer
    {
        public string Normalize(string email)
        {
            if (email == null) throw new ArgumentNullException(nameof(email));

            return email.ToUpper();
        }
    }
}