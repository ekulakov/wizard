using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Wizard.DataAccess;

namespace Wizard.Api.Services
{
    internal class ValidationService : IValidationService
    {
        private readonly DatabaseContext _context;

        private readonly IEmailNormalizer _emailNormalizer;

        public ValidationService(
            DatabaseContext context,
            IEmailNormalizer emailNormalizer)
        {
            _context = context;
            _emailNormalizer = emailNormalizer;
        }

        public async Task<bool> IsEmailFreeAsync(string email, CancellationToken token)
        {
            if (string.IsNullOrWhiteSpace(email)) throw new ArgumentNullException(nameof(email));

            var normalizedEmail = _emailNormalizer.Normalize(email);
            return await _context.Users
                       .Where(x => x.NormalizedEmail == normalizedEmail)
                       .Select(x => x.NormalizedEmail)
                       .CountAsync(token) == 0;
        }

        public async Task<bool> IsKnownProvince(int provinceId, CancellationToken token)
        {
            return await _context.Provinces
                       .Where(x => x.Id == provinceId)
                       .Select(x => x.Id)
                       .CountAsync(token) == 1;
        }
    }
}