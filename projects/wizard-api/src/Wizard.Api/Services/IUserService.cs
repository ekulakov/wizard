using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Wizard.Api.Models;
using Wizard.Domain;

namespace Wizard.Api.Services
{
    public interface IUserService
    {
        [ItemNotNull]
        Task<User> CreateUserAsync(
            [NotNull] CreateUserModel model,
            CancellationToken token = default);
    }
}