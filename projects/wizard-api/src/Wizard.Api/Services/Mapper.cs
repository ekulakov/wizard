using System;
using Wizard.Api.Models;
using Wizard.Domain;

namespace Wizard.Api.Services
{
    internal class Mapper : IMapper
    {
        public CountryListModel ToListModel(Country country)
        {
            if (country == null) throw new ArgumentNullException(nameof(country));

            return new CountryListModel
            {
                Id = country.Id,
                Name = country.Name,
            };
        }

        public ProvinceListModel ToListModel(Province province)
        {
            if (province == null) throw new ArgumentNullException(nameof(province));

            return new ProvinceListModel
            {
                Id = province.Id,
                Name = province.Name,
            };
        }

        public UserModel ToUserModel(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            return new UserModel
            {
                Id = user.Id,
                Email = user.Email,
            };
        }
    }
}