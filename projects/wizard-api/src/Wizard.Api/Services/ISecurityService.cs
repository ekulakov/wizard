using JetBrains.Annotations;

namespace Wizard.Api.Services
{
    public interface ISecurityService
    {
        [NotNull]
        string GenerateHash([NotNull] string password);

        bool ValidateHash([NotNull] string password, [NotNull] string savedPasswordHash);
    }
}