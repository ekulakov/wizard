using JetBrains.Annotations;

namespace Wizard.Api.Services
{
    public interface IEmailNormalizer
    {
        [NotNull]
        string Normalize([NotNull] string email);
    }
}