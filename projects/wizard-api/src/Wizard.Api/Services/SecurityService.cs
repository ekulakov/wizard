using System;
using System.Security.Cryptography;

namespace Wizard.Api.Services
{
    internal class SecurityService : ISecurityService
    {
        public string GenerateHash(string password)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));

            var salt = new byte[16];
            new RNGCryptoServiceProvider().GetBytes(salt);

            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            var hash = pbkdf2.GetBytes(20);

            var hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes);
        }

        public bool ValidateHash(string password, string savedPasswordHash)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (savedPasswordHash == null) throw new ArgumentNullException(nameof(savedPasswordHash));

            var hashBytes = Convert.FromBase64String(savedPasswordHash);
            var salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            var hash = pbkdf2.GetBytes(20);

            for (var i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != hash[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}