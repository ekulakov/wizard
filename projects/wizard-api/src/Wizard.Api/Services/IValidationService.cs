using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Wizard.Api.Services
{
    public interface IValidationService
    {
        Task<bool> IsEmailFreeAsync([NotNull] string email, CancellationToken token = default);

        Task<bool> IsKnownProvince(int provinceId, CancellationToken token = default);
    }
}