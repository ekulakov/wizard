using JetBrains.Annotations;
using Wizard.Api.Models;
using Wizard.Domain;

namespace Wizard.Api.Services
{
    public interface IMapper
    {
        [NotNull]
        CountryListModel ToListModel([NotNull] Country country);

        [NotNull]
        ProvinceListModel ToListModel([NotNull] Province province);        
        
        [NotNull]
        UserModel ToUserModel([NotNull] User user);
    }
}