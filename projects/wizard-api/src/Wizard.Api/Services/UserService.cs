using System;
using System.Threading;
using System.Threading.Tasks;
using Wizard.Api.Models;
using Wizard.DataAccess;
using Wizard.Domain;

namespace Wizard.Api.Services
{
    internal class UserService : IUserService
    {
        private readonly DatabaseContext _context;

        private readonly ISecurityService _securityService;

        public UserService(
            DatabaseContext context,
            ISecurityService securityService)
        {
            _context = context;
            _securityService = securityService;
        }

        public async Task<User> CreateUserAsync(CreateUserModel model, CancellationToken token)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));
            
            var user = new User
            {
                Email = model.Email,
                NormalizedEmail = model.Email.ToUpper(),
                ProvinceId = model.ProvinceId,
                Password = _securityService.GenerateHash(model.Password),
            };

            _context.Add(user);
            await _context.SaveChangesAsync(token);

            return user;
        }
    }
}