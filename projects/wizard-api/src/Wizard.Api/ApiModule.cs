using Autofac;
using FluentValidation;
using Wizard.Api.Models;
using Wizard.Api.Services;
using Wizard.Api.Validators;

namespace Wizard.Api
{
    public class ApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Mapper>().As<IMapper>();
            builder.RegisterType<SecurityService>().As<ISecurityService>();
            builder.RegisterType<ValidationService>().As<IValidationService>();
            builder.RegisterType<CreateUserModelValidator>().As<IValidator<CreateUserModel>>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<EmailNormalizer>().As<IEmailNormalizer>();
        }
    }
}