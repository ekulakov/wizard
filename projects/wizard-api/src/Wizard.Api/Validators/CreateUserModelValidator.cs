using System.Linq;
using FluentValidation;
using Wizard.Api.Models;
using Wizard.Api.Services;

namespace Wizard.Api.Validators
{
    internal class CreateUserModelValidator : AbstractValidator<CreateUserModel>
    {
        public CreateUserModelValidator(IValidationService validationService)
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress()
                .MustAsync(validationService.IsEmailFreeAsync).WithMessage("Email is taken");

            RuleFor(x => x.Password)
                .NotEmpty()
                .Must(x => x.Any(char.IsLetter)).WithMessage("Password must contain min 1 digit and min 1 letter")
                .Must(x => x.Any(char.IsDigit)).WithMessage("Password must contain min 1 digit and min 1 letter");

            RuleFor(x => x.PasswordConfirm)
                .Equal(x => x.Password).WithMessage("Confirm password must be the same with the password");

            RuleFor(x => x.ProvinceId)
                .InclusiveBetween(1, int.MaxValue)
                .MustAsync(validationService.IsKnownProvince).WithMessage("Province is unknown");
        }
    }
}