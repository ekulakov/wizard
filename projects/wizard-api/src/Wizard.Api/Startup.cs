﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Wizard.DataAccess;

namespace Wizard.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IContainer Container { get; private set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<DatabaseContext>(
                options =>
                {
                    var connectionString = Configuration.GetConnectionString("Wizard");
                    options.UseNpgsql(connectionString, x => x.CommandTimeout(1));
                });

            services.AddSingleton(Configuration);

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule<ApiModule>();
            Container = containerBuilder.Build();

            return new AutofacServiceProvider(Container);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseCors(b => b.AllowAnyMethod().AllowAnyHeader().WithOrigins("http://localhost:4200"));
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseCors(b => b.AllowAnyMethod().AllowAnyHeader().WithOrigins("http://localhost:6501"));
            }

            lifetime.ApplicationStopped.Register(() => Container?.Dispose());
            app.UseMvc();
        }
    }
}