using System;
using System.Collections.Generic;
using FluentValidation.Results;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Wizard.Api.Extensions
{
    public static class ValidatorExtensions
    {
        public static ModelStateDictionary ToModelState([NotNull] this IList<ValidationFailure> failures)
        {
            if (failures == null) throw new ArgumentNullException(nameof(failures));

            var state = new ModelStateDictionary();
            foreach (var failure in failures)
            {
                state.AddModelError(failure.PropertyName, failure.ErrorMessage);
            }

            return state;
        }
    }
}