using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Wizard.Api.Extensions;
using Wizard.Api.Models;
using Wizard.Api.Services;

namespace Wizard.Api.Controllers
{
    [Route("api/users")]
    public class UsersController : Controller
    {
        private readonly IMapper _mapper;

        private readonly IValidator<CreateUserModel> _userValidator;

        private readonly IUserService _userService;

        private readonly IValidationService _validationService;

        public UsersController(
            IMapper mapper,
            IValidator<CreateUserModel> userValidator,
            IUserService userService,
            IValidationService validationService)
        {
            _mapper = mapper;
            _userValidator = userValidator;
            _userService = userService;
            _validationService = validationService;
        }

        [HttpPost("create")]
        public async Task<ActionResult<UserModel>> CreateUserAsync(
            [FromBody] CreateUserModel model,
            CancellationToken token)
        {
            var validationResult = await _userValidator.ValidateAsync(model, token);
            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors.ToModelState());
            }

            var user = await _userService.CreateUserAsync(model, token);
            var userModel = _mapper.ToUserModel(user);

            return Ok(userModel);
        }

        [HttpPost("validate-email")]
        public async Task<ActionResult<ValidateEmailResponse>> ValidateEmailRequestAsync(
            [FromBody] ValidateEmailRequest request,
            CancellationToken token)
        {
            var isFree = await _validationService.IsEmailFreeAsync(request.Email, token);
            var response = new ValidateEmailResponse {IsFree = isFree};

            return Ok(response);
        }
    }
}