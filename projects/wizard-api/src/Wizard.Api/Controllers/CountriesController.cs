using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Wizard.Api.Models;
using Wizard.Api.Services;
using Wizard.DataAccess;

namespace Wizard.Api.Controllers
{
    [Route("api/countries")]
    public class CountriesController : Controller
    {
        private readonly DatabaseContext _context;

        private readonly IMapper _mapper;

        public CountriesController(
            DatabaseContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CountryListModel>>> GetCountriesAsync(CancellationToken token)
        {
            var countries = await _context.Countries
                .AsNoTracking()
                .ToListAsync(token);
            var models = countries.Select(x => _mapper.ToListModel(x));

            return Ok(models);
        }
    }
}