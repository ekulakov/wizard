using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Wizard.Api.Models;
using Wizard.Api.Services;
using Wizard.DataAccess;

namespace Wizard.Api.Controllers
{
    [Route("api/countries/{countryId}/provinces")]
    public class ProvincesController : Controller
    {
        private readonly DatabaseContext _context;

        private readonly IMapper _mapper;

        public ProvincesController(
            DatabaseContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProvinceListModel>>> GetProvinceAsync(
            int countryId,
            CancellationToken token)
        {
            var countries = await _context.Provinces
                .AsNoTracking()
                .Where(x => x.CountryId == countryId)
                .ToListAsync(token);
            var models = countries.Select(x => _mapper.ToListModel(x));

            return Ok(models);
        }
    }
}