﻿namespace Wizard.Domain
{
    public class User
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string NormalizedEmail { get; set; }

        public string Password { get; set; }
        
        public int ProvinceId { get; set; }

        public Province Province { get; set; }
    }
}