using System.Collections.Generic;

namespace Wizard.Domain
{
    public class Country
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Province> Provinces { get; set; } = new HashSet<Province>();
    }
}