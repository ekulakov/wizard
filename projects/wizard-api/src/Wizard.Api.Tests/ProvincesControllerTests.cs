using System.Threading.Tasks;
using FluentAssertions;
using Wizard.Api.Models;
using Wizard.Api.Tests.Utils;
using Xunit;

namespace Wizard.Api.Tests
{
    public class ProvincesControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public ProvincesControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task ShouldReturnProvincesByCountry()
        {
            // arrange
            var client = _factory.CreateClient();

            // act
            var listModels = await client
                .GetAsync("api/countries/1/provinces")
                .Success()
                .Response<ProvinceListModel[]>();

            // assert
            listModels.Should().BeEquivalentTo(new[]
            {
                new ProvinceListModel {Id = 1, Name = "Province 1"},
                new ProvinceListModel {Id = 2, Name = "Province 2"},
            });
        }
    }
}