using FluentAssertions;
using Wizard.Api.Services;
using Xunit;

namespace Wizard.Api.Tests
{
    public class SecurityServiceTests
    {
        private readonly SecurityService _securityService;

        public SecurityServiceTests()
        {
            _securityService = new SecurityService();
        }

        [Fact]
        public void ShouldGeneratePassword()
        {
            // arrange
            var password = "123qwe";
            
            // act
            var hash = _securityService.GenerateHash(password);
            
            // assert
            hash.Should().NotBeNull();
        }

        [Fact]
        public void ShouldGenerateDifferentHashes()
        {
            // arrange
            var password = "123qwe";

            // act
            var hash1 = _securityService.GenerateHash(password);
            var hash2 = _securityService.GenerateHash(password);

            // assert
            hash1.Should().NotBe(hash2);
        }

        [Fact]
        public void ShouldValidateHash()
        {
            // arrange
            var password = "123qwe";
            var hash = _securityService.GenerateHash(password);
            
            // act
            var isValid = _securityService.ValidateHash(password, hash);

            // assert
            isValid.Should().BeTrue();
        }

        [Fact]
        public void ShouldNotValidateHash()
        {
            // arrange
            var password1 = "123qwe";
            var password2 = "qwe123";
            var hash = _securityService.GenerateHash(password1);

            // act
            var isValid = _securityService.ValidateHash(password2, hash);

            // assert
            isValid.Should().BeFalse();
        }
    }
}