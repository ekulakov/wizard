using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;

namespace Wizard.Api.Tests.Utils
{
    public static class HttpResponseMessageExtensions
    {
        public static async Task<HttpResponseMessage> Success(
            this Task<HttpResponseMessage> request)
        {
            var message = await request;

            var readAsStringAsync = await message.Content.ReadAsStringAsync();
            message.IsSuccessStatusCode.Should().BeTrue(readAsStringAsync);

            return message;
        }

        public static async Task<HttpResponseMessage> Status(
            this Task<HttpResponseMessage> request,
            HttpStatusCode code)
        {
            var message = await request;

            message.StatusCode.Should().Be(code);

            return message;
        }

        public static async Task<TResponse> Response<TResponse>(
            this Task<HttpResponseMessage> request)
        {
            var message = await request;
            return await message.Content.ReadAsAsync<TResponse>();
        }
    }
}