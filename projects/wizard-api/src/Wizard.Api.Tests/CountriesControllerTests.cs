using System.Threading.Tasks;
using FluentAssertions;
using Wizard.Api.Models;
using Wizard.Api.Tests.Utils;
using Xunit;

namespace Wizard.Api.Tests
{
    public class CountriesControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public CountriesControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task ShouldReturnProvincesByCountry()
        {
            // arrange
            var client = _factory.CreateClient();

            // act
            var listModels = await client
                .GetAsync("api/countries")
                .Success()
                .Response<CountryListModel[]>();

            // assert
            listModels.Should().BeEquivalentTo(new[]
            {
                new CountryListModel {Id = 1, Name = "Country 1"},
                new CountryListModel {Id = 2, Name = "Country 2"},
            });
        }
    }
}