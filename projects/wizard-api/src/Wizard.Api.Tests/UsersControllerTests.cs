using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Wizard.Api.Models;
using Wizard.Api.Tests.Utils;
using Wizard.DataAccess;
using Xunit;

namespace Wizard.Api.Tests
{
    public class UsersControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public UsersControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task ShouldCreateUser()
        {
            // arrange
            var client = _factory.CreateClient();

            var createUserModel = new CreateUserModel
            {
                Email = "alice@mail.ru",
                Password = "123qwe",
                PasswordConfirm = "123qwe",
                ProvinceId = 1,
            };

            // act
            var userModel = await client
                .PostAsJsonAsync("api/users/create", createUserModel)
                .Success()
                .Response<UserModel>();

            // assert
            userModel.Should().BeEquivalentTo(new UserModel
            {
                Email = "alice@mail.ru",
            }, o => o.Excluding(x => x.Id));

            var context = _factory.Server.Host.Services.GetRequiredService<DatabaseContext>();
            var user = await context.Users.FirstOrDefaultAsync(x => x.Id == userModel.Id);
            user.Should().NotBeNull();
            user.Id.Should().BeGreaterThan(0);
            user.Password.Should().NotBeNull();
            user.Email.Should().Be("alice@mail.ru");
            user.NormalizedEmail.Should().Be("ALICE@MAIL.RU");
        }

        [Fact]
        public async Task EmailShouldBeTaken()
        {
            // arrange
            var client = _factory
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        var provider = services.BuildServiceProvider();
                        var db = provider.GetRequiredService<DatabaseContext>();
                        db.Users.RemoveRange(db.Users);
                        db.SaveChanges();
                    });
                })
                .CreateClient();

            await client
                .PostAsJsonAsync("api/users/create", new CreateUserModel
                {
                    Email = "alice@mail.ru",
                    Password = "123qwe",
                    PasswordConfirm = "123qwe",
                    ProvinceId = 1,
                })
                .Success();

            // act
            var response = await client
                .PostAsJsonAsync("api/users/validate-email", new ValidateEmailRequest
                {
                    Email = "alice@mail.ru",
                })
                .Success()
                .Response<ValidateEmailResponse>();

            // assert
            response.Should().BeEquivalentTo(new ValidateEmailResponse
            {
                IsFree = false,
            });
        }

        [Fact]
        public async Task EmailShouldBeFree()
        {
            // arrange
            var client = _factory
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        var provider = services.BuildServiceProvider();
                        var db = provider.GetRequiredService<DatabaseContext>();
                        db.Users.RemoveRange(db.Users);
                        db.SaveChanges();
                    });
                })
                .CreateClient();

            // act
            var response = await client
                .PostAsJsonAsync("api/users/validate-email", new ValidateEmailRequest
                {
                    Email = "alice@mail.ru",
                })
                .Success()
                .Response<ValidateEmailResponse>();

            // assert
            response.Should().BeEquivalentTo(new ValidateEmailResponse
            {
                IsFree = true,
            });
        }

        [Fact]
        public async Task ShouldFailIfEmailIsTaken()
        {
            // arrange
            var client = _factory
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        var provider = services.BuildServiceProvider();
                        var db = provider.GetRequiredService<DatabaseContext>();
                        db.Users.RemoveRange(db.Users);
                        db.SaveChanges();
                    });
                })
                .CreateClient();

            var createUserModel = new CreateUserModel
            {
                Email = "alice@mail.ru",
                Password = "123qwe",
                PasswordConfirm = "123qwe",
                ProvinceId = 1,
            };

            await client
                .PostAsJsonAsync("api/users/create", createUserModel)
                .Success();

            // act
            var state = await client
                .PostAsJsonAsync("api/users/create", createUserModel)
                .Status(HttpStatusCode.BadRequest)
                .Response<ValidationError>();

            // assert
            var expected = new ModelStateDictionary();
            expected.AddModelError("Email", "Email is taken");
            state.Should().BeEquivalentTo(new ValidationError
            {
                {"Email", new[] {"Email is taken"}}
            });
        }
    }
}